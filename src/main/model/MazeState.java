package model;

import javafx.scene.paint.Color;

public enum MazeState {
    OPEN {
        @Override
        public Color getColor() {
            return Color.WHITESMOKE;
        }
        @Override
        public boolean visited() {
            return false;
        }
    }, CLOSED {
        @Override
        public Color getColor() {
            return Color.DARKGRAY;
        }
        @Override
        public boolean visited() {
            return false;
        }
    }, VISITED {
        @Override
        public Color getColor() {
            return Color.LIGHTBLUE;
        }
        @Override
        public boolean visited() {
            return true;
        }
    }, CURRENT {
        @Override
        public Color getColor() {
            return Color.RED;
        }
        @Override
        public boolean visited() {
            return true;
        }
    };


    abstract public Color getColor();
    abstract public boolean visited();
}

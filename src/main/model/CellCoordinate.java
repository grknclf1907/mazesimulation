package model;

import static gui.MazeController.cellSize;

public class CellCoordinate {
    private int x, y;

    public CellCoordinate(int x, int y) {
        this.x = x * cellSize;
        this.y = y * cellSize;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof CellCoordinate) {
            CellCoordinate that = (CellCoordinate) other;
            return this.x == that.x && this.y == that.y;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

}

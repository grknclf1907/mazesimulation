package model;

public class MazeDetails {
    private String generationMethod;
    private String solvingMethod;
    public MazeDetails(){

    }
    public MazeDetails(int mazeWidth, int mazeHeight){
    }
    public MazeDetails(String generationMethod, String solvingMethod){
        this.generationMethod = generationMethod;
        this.solvingMethod = solvingMethod;
    }

    public String getGenerationMethod() {
        return generationMethod;
    }

    public void setGenerationMethod(String generationMethod) {
        this.generationMethod = generationMethod;
    }

    public String getSolvingMethod() {
        return solvingMethod;
    }

    public void setSolvingMethod(String solvingMethod) {
        this.solvingMethod = solvingMethod;
    }

}

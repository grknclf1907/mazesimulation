package model;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import java.util.ArrayList;
import java.util.List;
import static gui.MazeController.*;

public class CellModel {
    private Pane pane;
    private int thisRow;
    private int thisCol;
    private CellCoordinate cellCoordinate;
    private ArrayList<CellModel> neighbors = new ArrayList<>();
    private ArrayList<CellModel> solvingNeighbors = new ArrayList<>();
    private SolveState solvingBlock;
    private MazeState cellBlock;

    public CellModel(GridPosition gridPosition, MazeState cellBlock, SolveState solvingBlock,Pane pane) {
        cellCoordinate = new CellCoordinate(gridPosition.getRow(), gridPosition.getCol());
        thisRow = gridPosition.getRow();
        thisCol = gridPosition.getCol();
        this.solvingBlock = solvingBlock;
        this.cellBlock = cellBlock;
        this.pane = pane;
    }

    /**
     * ---------------------
     * Getters and Setters
     *
     * Cell Specific
     * ---------------------
     */
    public int getX() {
        return cellCoordinate.getX();
    }

    public int getY() {
        return cellCoordinate.getY();
    }

    public int getThisRow() {
        return thisRow;
    }

    public int getThisCol() {
        return thisCol;
    }

    public List<CellModel> getNeighbors(){
        return neighbors;
    }

    /**
     * ----------------
     * Maze Variables
     * ----------------
     */
    private Rectangle rectangle;
    /**
     * ---------------------
     * Getters and Setters
     *
     * MazeState Specific
     * ---------------------
     */

    public void setMazeState(){
        rectangle = new Rectangle(getX(), getY(), cellSize, cellSize);
        rectangle.setFill(Color.PURPLE);
        pane.getChildren().add(rectangle);
    }
    public MazeState getCellState() {
        return cellBlock;
    }

    public void setCellState(MazeState cellBlock) {
        this.cellBlock = cellBlock;
    }

    /**
     * ----------------
     * Wall Variables
     * ----------------
     */
    private boolean[] walls = new boolean[]{true, true, true, true};//N,E,S,W
    private Line lineN;
    private Line lineE;
    private Line lineS;
    private Line lineW;


    /**
     * ---------------------
     * Getters and Setters
     *
     * Wall Specific
     * ---------------------
     */

    public void setNorthWall(boolean open) {
        this.walls[0] = open;
    }

    public void setEastWall(boolean open) {
        this.walls[1] = open;
    }

    public void setSouthWall(boolean open) {
        this.walls[2] = open;
    }

    public void setWestWall(boolean open) {
        this.walls[3] = open;
    }

    public boolean getNorthWall() {
        return walls[0];
    }

    public boolean getEastWall() {
        return walls[1];
    }

    public boolean getSouthWall() {
        return walls[2];
    }

    public boolean getWestWall() {
        return walls[3];
    }


    /**
     * ----------------
     * Solving Variables
     * ----------------
     */

    //g is the starting value --> initial value is 12 0's
    private double g_Score = 1000000000000.0;
    //heuristic_Value can be calculated to be a straight-line distance OR Manhatten distance
    private double heuristic_Score = 1000000000000.0;
    ;
    //lowest f value is the end goal.
    private double f_Score;
    //OR could use a Hashmap!! instead of previous
    private CellModel previous;
    private Line linePath;

    /**
     * ---------------------
     * Getters and Setters
     *
     * Solving Specific
     * ---------------------
     */
    public void setG_Score(double g_Score){
        this.g_Score = g_Score;
    }

    public void setHeuristic_Score(double heuristic_Score){
        this.heuristic_Score = heuristic_Score;
    }
    public void setF_Score(double f_Score){
        this.f_Score = f_Score;
    }

    public double getG_Score(){
        return g_Score;
    }

    public double getHeuristic_Score(){
        return heuristic_Score;
    }
    public double getF_Score(){
        return f_Score;
    }
    public void setPreviousCell(CellModel previous){
        this.previous = previous;
    }
    public CellModel getPreviousCell(){
        return previous;
    }

    public void setLinePath(){
        this.linePath = linePath;
    }
    public Line getLinePath(){
        return linePath;
    }

    public SolveState getSolvingState() {
        return solvingBlock;
    }

    public void setSolvingState(SolveState solvingBlock) {
        this.solvingBlock = solvingBlock;
    }
    public List<CellModel> getSolvingNeighbors(){
        return solvingNeighbors;
    }

    /**
     * ---------------------
     * METHODS
     * ---------------------
     */
    private void checkNorthWall() {
        if (getNorthWall() == true) {
            lineN = new Line(getX(), getY(), getX() + cellSize, getY());
            lineN.setStrokeWidth(1.5);
            pane.getChildren().add(lineN);
        } else if (getNorthWall() == false && !(lineN == null)) {
            pane.getChildren().remove(lineN);
        }
    }

    private void checkEastWall() {
        if (getEastWall() == true) {
            lineE = new Line(getX() + cellSize, getY(), getX() + cellSize, getY() + cellSize);
            lineE.setStrokeWidth(1.5);
            pane.getChildren().add(lineE);
        } else if (getEastWall() == false && !(lineE == null)) {
            pane.getChildren().remove(lineE);
        }
    }

    private void checkSouthWall() {
        if (getSouthWall() == true) {
            lineS = new Line(getX() + cellSize, getY() + cellSize, getX(), getY() + cellSize);
            lineS.setStrokeWidth(1.5);
            pane.getChildren().add(lineS);
        } else if (getSouthWall() == false && !(lineS == null)) {
            pane.getChildren().remove(lineS);
        }
    }

    private void checkWestWall() {
        if (getWestWall() == true) {
            lineW = new Line(getX(), getY() + cellSize, getX(), getY());
            lineW.setStrokeWidth(1.5);
            pane.getChildren().add(lineW);
        } else if (getWestWall() == false && !(lineW == null)) {
            pane.getChildren().remove(lineW);
        }
    }

    @Override
    public String toString() {
        return ("Cell X:" + getX() + "," +
                " Cell Y: " + getY());
    }

    public void drawLinePath(CellModel to){
        int x1 = getX() + cellSize/2;
        int y1 = getY() + cellSize/2;
        int x2 = to.cellCoordinate.getX() + cellSize/2;
        int y2 = to.cellCoordinate.getY() + cellSize/2;

        Line currentLine = new Line(x1, y1, x2, y2);
        currentLine.setStrokeWidth(5);
        currentLine.setStroke(Color.RED);

        pane.getChildren().add(currentLine);
    }

    public void drawMazePath() {
        rectangle = new Rectangle(getX(), getY(), cellSize, cellSize);
        setCellState(cellBlock);
        rectangle.setFill(this.getCellState().getColor());

        pane.getChildren().add(rectangle);
        drawWalls();
    }

    public void drawSolutionPath(){
        rectangle = new Rectangle(getX(), getY(), cellSize, cellSize);
        setSolvingState(solvingBlock);
        rectangle.setFill(getSolvingState().getColor());

        pane.getChildren().add(rectangle);
        drawWalls();
    }

    private void drawWalls(){
        checkNorthWall();
        checkEastWall();
        checkSouthWall();
        checkWestWall();
    }

    public void drawAll() {
        drawMazePath();
        drawSolutionPath();
    }
}

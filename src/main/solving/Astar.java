package solving;

import javafx.scene.shape.Line;
import model.CellModel;
import model.GridPosition;
import model.SolveState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Astar {
    private CellModel current_cell;
    private CellModel starting_cell;
    private CellModel ending_cell;
    //OR could use a Hashmap!! instead of previous
    private CellModel previous;
    private HashMap<GridPosition, CellModel> gridMap;
    private ArrayList<CellModel> actualPath = new ArrayList<>();
    private ArrayList<CellModel> searchPath = new ArrayList<>(); // for animation
    private ArrayList<CellModel> openSet = new ArrayList<>();
    //private ArrayList<CellModel> solvingNeighbors = new ArrayList<>();
    private boolean searchedNeighborsAdded = false;
    private boolean pathFound = false;

    //g is the starting value --> initial value is 12 0's
    private double g_Score = 1000000000000.0;
    //heuristic_Value can be calculated to be a straight-line distance OR Manhatten distance
    private double heuristic_Score = 1000000000000.0;
    //lowest f value is the end goal.
    private double f_Score;
    private Line linePath;

    public Astar(HashMap<GridPosition, CellModel> gridMap, CellModel starting_cell, CellModel ending_cell) {
        this.gridMap = gridMap;
        this.starting_cell = starting_cell;
        this.current_cell = starting_cell;
        this.ending_cell = ending_cell;
    }

    /**
     * ---------------------
     * Getters and Setters
     * ---------------------
     */
    public void setSolvingComplete(boolean pathFound) {
        this.pathFound = pathFound;
    }

    public boolean isSolvingComplete() {
        return pathFound;
    }

    public CellModel getSolvingMapCell(int row, int col) {
        for (Map.Entry<GridPosition, CellModel> loopCell : gridMap.entrySet()) {
            CellModel cm;

            cm = loopCell.getValue();

            if (cm.getThisRow() == row && cm.getThisCol() == col) {
                return cm;
            }
        }
        System.out.println("ERROR IN: =Astar= getSolvingMapCell()");
        return null;
    }

    /**
     * ---------------------
     * METHODS
     * ---------------------
     */
    public void aStarSolvingAlgorithm() {
        //this loop will continuously keep being updated. if searchedNeighbors
        if (!searchedNeighborsAdded) {
            for (Map.Entry<GridPosition, CellModel> loopCell : gridMap.entrySet()) {
                CellModel cm;
                cm = loopCell.getValue();

                addSolvingNeighbors(cm, cm.getThisRow(), cm.getThisCol());
            }
            //if not added.. add to openSet
            starting_cell.setG_Score(0);
            //add 1 to Fscore if you have problems
            starting_cell.setF_Score(heuristic_Manhatten(starting_cell, ending_cell));
            openSet.add(starting_cell);
            searchedNeighborsAdded = true;
        }
        //find node with least F score
        //set current_cell to that block
        if (openSet.size() > 0) {
            if(current_cell != null){
                getSolvingMapCell(current_cell.getThisRow(),current_cell.getThisCol()).setSolvingState(SolveState.VISITED);
            }

            current_cell = getSolvingMapCell(lowestFOpenSet().getThisRow(), lowestFOpenSet().getThisCol());
            getSolvingMapCell(current_cell.getThisRow(),current_cell.getThisCol()).setSolvingState(SolveState.CURRENT);

            //if current == ending_cell that means you've reached your goal
            if (current_cell == ending_cell) {
                System.out.println("Path Found");
                this.setSolvingComplete(true);
                //now that path has been found.. build ACTUAL PATH
                //draw line from start to finish.
                actualPath = reconstructActualPath();
                for (int i = 0; i < actualPath.size() - 1; i++) {

                    getSolvingMapCell(actualPath.get(i).getThisRow(), actualPath.get(i).getThisCol())
                            .setSolvingState(SolveState.VISITED);

//                    getSolvingMapCell(actualPath.get(i).getThisRow(), actualPath.get(i).getThisCol())
//                            .drawActualPath(actualPath.get(i + 1));
                }
            }
            //searching through solvingNeighbors list
            if (!pathFound) {
                openSet.remove(current_cell);
                for (CellModel neighborCell : getSolvingMapCell(current_cell.getThisRow(), current_cell.getThisCol())
                        .getSolvingNeighbors()) {
                    double tentative_GScore = getSolvingMapCell(current_cell.getThisRow(), current_cell.getThisCol())
                            .getG_Score() + distance(current_cell, neighborCell);
                    if (tentative_GScore < neighborCell.getG_Score()) {
                        neighborCell.setPreviousCell(current_cell);
                        neighborCell.setG_Score(tentative_GScore);
                        //edge value
                        neighborCell.setF_Score(neighborCell.getG_Score() + heuristic_Manhatten(neighborCell, ending_cell));

                        if (!openSet.contains(neighborCell)) {
                            openSet.add(neighborCell);
                        }
                    }
                }
                reconstructSearchPath();
                drawSearchPath();
            }
        }
        draw();
    }

    private double heuristic_Manhatten(CellModel from, CellModel to) {
        double distance = 0;
        distance = distance(from, to);
        return distance;
    }

    private CellModel lowestFOpenSet() {
        CellModel lowestFScore = openSet.get(0);
        for (CellModel cell : openSet) {
            if (cell.getF_Score() < lowestFScore.getF_Score()) {
                lowestFScore = getSolvingMapCell(cell.getThisRow(), cell.getThisCol());
            }
        }
        return lowestFScore;
    }
    private double distance(CellModel from, CellModel to) {
        return Math.sqrt(Math.pow(from.getX() - to.getX(), 2) + Math.pow(from.getY() - to.getY(), 2));
    }
    //finding the path from where you are searching FROM, TO the starting_cell
    public ArrayList<CellModel> reconstructSearchPath() {
        CellModel current = getSolvingMapCell(current_cell.getThisRow(), current_cell.getThisCol());

        searchPath.add(current);
        while (current != starting_cell) {
            searchPath.add(current);
            current = current.getPreviousCell();
        }
        return searchPath;
    }

    public ArrayList<CellModel> reconstructActualPath() {
        CellModel current = getSolvingMapCell(current_cell.getThisRow(), current_cell.getThisCol());

        actualPath.add(current);
        while (current != starting_cell) {
            actualPath.add(current);
            current = current.getPreviousCell();
        }
        return actualPath;
    }

    public boolean getMazeNorthWall(CellModel cell){
        boolean isWall = getSolvingMapCell(cell.getThisRow(), cell.getThisCol()).getNorthWall();
        return isWall;
    }
    public boolean getMazeSouthWall(CellModel cell){
        boolean isWall = getSolvingMapCell(cell.getThisRow(), cell.getThisCol()).getSouthWall();
        return isWall;
    }
    public boolean getMazeEastWall(CellModel cell){
        boolean isWall = getSolvingMapCell(cell.getThisRow(), cell.getThisCol()).getEastWall();
        return isWall;
    }
    public boolean getMazeWestWall(CellModel cell){
        boolean isWall = getSolvingMapCell(cell.getThisRow(), cell.getThisCol()).getWestWall();
        return isWall;
    }

    public void addSolvingNeighbors(CellModel cell,int thisRow, int thisCol) {

        //add TOP BOUND.
        if (!getMazeSouthWall(cell)) {
            //add lower cell
            cell.getSolvingNeighbors().add(getSolvingMapCell(thisRow, thisCol + 1));
        }
        //add RIGHT BOUND
        if (!getMazeWestWall(cell)) {
            //add left cell
            cell.getSolvingNeighbors().add(getSolvingMapCell(thisRow - 1, thisCol));
        }
        //add LOWER BOUND.
        if (!getMazeNorthWall(cell)) { //if line of south wall is not there. add south position.
            //add upper cell
            cell.getSolvingNeighbors().add(getSolvingMapCell(thisRow, thisCol - 1));
        }
        //add LEFT BOUND.
        if (!getMazeEastWall(cell)) {
            //add right
            cell.getSolvingNeighbors().add(getSolvingMapCell(thisRow + 1, thisCol));
        }
    }
    public void drawMap() {
        for (Map.Entry<GridPosition, CellModel> cellLoop : gridMap.entrySet()) {
            CellModel cm;
            cm = cellLoop.getValue();

            cm.drawSolutionPath();
        }
    }

    private void drawSearchPath() {
        for (CellModel loopCell : searchPath) {
            loopCell.drawSolutionPath();
        }
    }

//    private void drawActualPath() {
//        for (int i = 0; i < actualPath.size() - 1; i++) {
//            getSolvingMapCell(actualPath.get(i).getThisRow(), actualPath.get(i).getThisCol()).drawActualPath(actualPath.get(i + 1));
//        }
//    }
    public void draw() {
        drawMap();
        drawSearchPath();
        //drawActualPath();
    }
}

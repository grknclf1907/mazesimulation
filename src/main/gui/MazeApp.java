package gui;
/**
 * Alexander Alvara
 * Maze Simulation
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * This class is responsible for running the maze.
 */
public class MazeApp extends Application {
    public static Stage stg;
    @Override
    public void start(Stage primaryStage) throws IOException {
        stg = primaryStage;

        Parent root = FXMLLoader.load(getClass().getResource("startPage.fxml"));

        Scene scene = new Scene(root);
        stg.setWidth(600);
        stg.setHeight(600);
        primaryStage.setTitle("Maze Generator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void changeScene(String fxml) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource(fxml));
        stg.getScene().setRoot(root);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

package gui;

import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.MazeDetails;
import model.MazeSimulation;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * This class directs the pages.
 */
public class MazeController implements Initializable {
    /**
     * ---------------
     * FXML variables
     * -----------------
     */
    @FXML
    Pane paneMazeSimulation;
    @FXML
    Button buttonBeginMaze;
    @FXML
    Button buttonStepMaze;
    @FXML
    Button buttonStartMaze;
    @FXML
    Button buttonStopMaze;
    @FXML
    Button buttonRestart;
    @FXML
    Label labelMazeSpeed;
    @FXML
    Label labelMouseSpeed;
    @FXML
    Label labelGenerationMethod;
    @FXML
    Label labelSolverMethod;
    @FXML
    Slider sliderMouseSpeed;
    @FXML
    Slider sliderMazeSpeed;

    /**
     * ------------------
     * local variables
     * --------------------
     */
    private MazeSimulation sim;
    public static final int mazeWidth = 10;
    public static final int mazeHeight = 10;
    public static int cellSize = 50;
    private Movement movement;

    /**
     * -----------------------
     * Animation timer Class
     * ------------------------
     */
    private class Movement extends AnimationTimer {
        public static long FRAMES_PER_SECOND = 60L;
        private long INTERVAL = 1000000000L / FRAMES_PER_SECOND;
        private long last = 0;

        @Override
        public void handle(long now) {
            if (now - last > INTERVAL) {
                step();
                last = now;
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //setting background color
        paneMazeSimulation.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

        //setting up listeners for sliders.
        sliderMouseSpeed.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setMouseSpeed();
            }
        });
        sliderMazeSpeed.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setMazeSpeed();
            }
        });
        movement = new Movement();
    }
    /**------------------
     * Getters & Setters
     ------------------*/

    /**
     * -------------
     * Methods
     * ----------------
     */
    @FXML
    private void begin() {
        sim = new MazeSimulation(paneMazeSimulation);
        paneMazeSimulation.getChildren().clear();
        sim.generateGridPositions();
        sim.generateMazePath();
        sim.generateMazeBuilding();
        sim.generateSolvingPath();
        sim.generateSolvingAlgorithm();
    }
    @FXML
    private void start() {
        movement.start();
        disableButtons(true, false, true);
    }
    @FXML
    private void stop() {
        movement.stop();
        disableButtons(false, true, false);
    }
    @FXML
    private void step() {
        sim.move();
    }
    @FXML
    private void restart() throws IOException {
        MazeApp mazeGenerator = new MazeApp();
        mazeGenerator.changeScene("startPage.fxml");
    }
    private void disableButtons(boolean start, boolean stop, boolean step) {
        buttonStartMaze.setDisable(start);
        buttonStopMaze.setDisable(stop);
        buttonStepMaze.setDisable(step);
    }
    public void initData(MazeDetails mazeDetails) {
        labelGenerationMethod.setText(mazeDetails.getGenerationMethod());
        labelSolverMethod.setText(mazeDetails.getSolvingMethod());
    }
    private void setMouseSpeed() {
        //Movement.FRAMES_PER_SECOND = (long) sliderMouseSpeed.getValue();
    }
    //as of right now.. animation is set by FRAMES_PER_SECOND in movement
    private void setMazeSpeed() {
        //Movement.FRAMES_PER_SECOND = (long) sliderMazeSpeed.getValue();
    }
}

package gui;

import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.MazeDetails;
import model.MazeSimulation;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MazeGenerationPageController implements Initializable {
    /**
     * ---------------
     * FXML variables
     * -----------------
     */
    @FXML
    Pane paneMazeSimulation;
    @FXML
    Button buttonBegin;
    @FXML
    Button buttonStep;
    @FXML
    Button buttonStart;
    @FXML
    Button buttonStop;
    @FXML
    Button buttonRestart;
    @FXML
    Slider sliderMazeSpeed;

    @FXML
    Label labelBuildingMethod;

    /**
     * ------------------
     * local variables
     * --------------------
     */
    private MazeSimulation sim;
    private Movement movement;

    /**
     * -----------------------
     * Animation timer Class
     * ------------------------
     */
    private class Movement extends AnimationTimer {
        public static long FRAMES_PER_SECOND = 60L;
        private long INTERVAL = 1000000000L / FRAMES_PER_SECOND;
        private long last = 0;

        @Override
        public void handle(long now) {
            if (now - last > INTERVAL) {
                step();
                last = now;
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //setting background color
        paneMazeSimulation.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

        sliderMazeSpeed.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                setMazeSpeed();
            }
        });
        movement = new Movement();
    }

    /**
     * -------------
     * Methods
     * ----------------
     */
    @FXML
    private void begin() {
        sim = new MazeSimulation(paneMazeSimulation);
        paneMazeSimulation.getChildren().clear();
        sim.generateGridPositions();
        sim.generateMazePath();
        //sim.generateBuilding();
    }
    @FXML
    private void start() {
        movement.start();
        disableButtons(true, false, true);
    }
    @FXML
    private void stop() {
        movement.stop();
        disableButtons(false, true, false);
    }
    @FXML
    private void step() {
        sim.moveBuilding();
    }
    @FXML
    private void restart() throws IOException {
        MazeApp mazeGenerator = new MazeApp();
        mazeGenerator.changeScene("startPage.fxml");
    }
    private void disableButtons(boolean start, boolean stop, boolean step) {
        buttonStart.setDisable(start);
        buttonStop.setDisable(stop);
        buttonStep.setDisable(step);
    }
    public void initData(MazeDetails mazeDetails) {
        labelBuildingMethod.setText(mazeDetails.getGenerationMethod());
    }
    //as of right now.. animation is set by FRAMES_PER_SECOND in movement
    private void setMazeSpeed() {
        //Movement.FRAMES_PER_SECOND = (long) sliderMazeSpeed.getValue();
    }

}

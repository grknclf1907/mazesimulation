package gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.MazeDetails;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class StartPageController implements Initializable {
    /**
     * ---------------
     * FXML variables
     * -----------------
     */
    @FXML
    Button buttonAllMaze;
    @FXML
    Button buttonBuildingMaze;
    @FXML
    ChoiceBox<String> choiceBoxGenerator;
    @FXML
    ChoiceBox<String> choiceBoxSolver;
    @FXML
    Label labelGenerator;
    @FXML
    Label labelSolver;


    /**
     * ------------------
     * local variables
     * --------------------
     */

    String[] generatorAlgorithms = {"randomDFS","randomKruskal","randomPrim","aldousBroder","recursiveDivision"};
    String[] solvingAlgorithms = {"randomMouse","wallFlower","pledge","tremaux","mazeRouting","aStar","lightning"};

    private MazeDetails selectedMazeDetails;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        choiceBoxGenerator.getItems().addAll(generatorAlgorithms);
        choiceBoxSolver.getItems().addAll(solvingAlgorithms);
    }

    /**
     * this is where the ID's are named to call the FXML files.
     */
    @FXML
    private void goToMazesPage(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("mazePage.fxml"));
        Parent mazeViewParent = loader.load();

        Scene sceneMazeView = new Scene(mazeViewParent);

        //access the controller and call a method
        gui.MazeController mazeController = loader.getController();


        selectedMazeDetails = new MazeDetails();
        selectedMazeDetails.setGenerationMethod(choiceBoxGenerator.getValue());
        selectedMazeDetails.setSolvingMethod(choiceBoxSolver.getValue());
        mazeController.initData(selectedMazeDetails);

        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(sceneMazeView);
        window.show();
    }

    @FXML
    private void goToMazesGenerationPage(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("mazeGeneration.fxml"));
        Parent mazeViewParent = loader.load();

        Scene sceneMazeView = new Scene(mazeViewParent);

        //access the controller and call a method
        gui.MazeGenerationPageController mazeGenerationPageController = loader.getController();


        selectedMazeDetails = new MazeDetails();
        selectedMazeDetails.setGenerationMethod(choiceBoxGenerator.getValue());
        selectedMazeDetails.setSolvingMethod(choiceBoxSolver.getValue());
        mazeGenerationPageController.initData(selectedMazeDetails);

        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(sceneMazeView);
        window.show();
    }

    @FXML
    private void goToMazesSolvingPage(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("mazeSolver.fxml"));
        Parent mazeViewParent = loader.load();

        Scene sceneMazeView = new Scene(mazeViewParent);

        //access the controller and call a method
        gui.MazeSolverPageController mazeSolverPageController = loader.getController();


        selectedMazeDetails = new MazeDetails();
        selectedMazeDetails.setGenerationMethod(choiceBoxGenerator.getValue());
        selectedMazeDetails.setSolvingMethod(choiceBoxSolver.getValue());
        mazeSolverPageController.initData(selectedMazeDetails);

        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setScene(sceneMazeView);
        window.show();
    }
}
